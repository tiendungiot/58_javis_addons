## 1.1.7:
- Cải tiến chức năng học lệnh RF
- Hỗ trợ tính năng cập nhật smart ir
- Bổ sung các dịch vụ phân tích HC và xoá log giải phóng dung lượng

## 1.1.6:
- Cải tiến chức năng học lệnh Broadlink
- Cải tiến chức năng cấu hình telegram, hỗ trợ group thông báo

## 1.1.5:
- Fix lỗi chức năng học lệnh Broadlink
- Fix lỗi cấu hình Spotify, Telegram

## 1.0.9:
- Cải tiến chức năng học lệnh Broadlink
- Hỗ trợ cấu hình Spotify
- Hỗ trợ cấu hình Telegram
- Fix lỗi tự động hoá kết hợp khoá cửa

## 1.0.8
- Bỏ phần cập nhật phiên bản mới trong menu Hệ thống
- Fix lỗi login khi HC có ảnh trong thư mục /tmp/camera
- Fix tạo công tắc mqtt từ công tắc wifi

## 1.0.1
- Phiên bản đầu tiên